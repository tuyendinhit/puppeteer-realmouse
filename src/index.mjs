import { visualHandler } from './MouseVisuals.mjs';
import { path, tracePath } from './MousePath.mjs';

export class Mouse {
    constructor(page) {
        this.page = page;
        this.lastPosition = {
            x: null,
            y: null,
        };
    }

    /**
     * Move the mouse to an element identified by `selector`, where `selector`
     * is one of:
     *
     *  - Dictionary of `x` and `y` coordinates
     *  - HTML element (handle)
     *  - Selector (string)
     *  - XPath selector (string)
     *
     * Options via `options` dictionary are:
     *
     *  - immediate: set to `true` to move the mouse immediately
     *  - waitForSelector (optional, number)
     *  - waitBefore: wait X ms before starting to move the mouse
     *  - waitAfter wait X ms after the mouse arrived to the destination
     *
     * Returns:
     *
     *  - The handle of the HTML element the mouse was moved to or, the
     *    coordinates as a dictionary if moveTo was called with coords.
     *
     */
    moveTo = async (selector, options = {}) => {
        let target = null;
        let coords = null;
        if (selector.constructor == Object && selector.x && selector.y) {
            coords = selector;
        } else {
            target = await this.#resolveTarget(
                selector,
                options.waitForSelector
            );
            if (!target) {
                throw Error(
                    `Could not find element with selector "${selector}", pass 'options.waitForSelector' to 'moveTo' or wait for the elements with 'puppeteer.waitForSelector'.`
                );
            }
            await this.#ensureElementInView(target);
            coords = await this.#getMouseDestination(target);
        }
        await this.#delay(options.waitBefore || 0);
        const { x, y } = this.#randomizeLandingPoint(coords, options);
        await this.#moveMouseToPosition(x, y, options);
        await this.#delay(options.waitAfter || 0);
        return target || coords;
    };

    install = async () => {
        await this.moveTo('html', { immediate: true });
        await this.#installMouseVisual();
    };

    #delay = async (ms) => {
        return this.page.waitForTimeout(ms);
    };

    #randomizeLandingPoint = (coords, options) => {
        if (options.immediate) return coords;
        const { top, left, bottom, right } = coords.box;
        const offsetX = Math.random() * ((right - left) / 2);
        const offsetY = Math.random() * ((bottom - top) / 2);
        const shouldAdd = Math.random() * 1000 > 500;
        return {
            x: shouldAdd ? coords.x + offsetX : coords.x - offsetX,
            y: shouldAdd ? coords.y + offsetY : coords.y - offsetY,
        };
    };

    #ensureElementInView = async (target) => {
        if (!target) return;
        await this.page._client.send('DOM.scrollIntoViewIfNeeded', {
            objectId: target._remoteObject.objectId,
        });
    };

    #resolveTarget = async (selector, waitForSelector) => {
        if (selector.constructor !== String) return selector;
        if (selector.startsWith('//') || selector.startsWith('(//')) {
            return this.#resolveTargetFromXPathSelector(
                selector,
                waitForSelector
            );
        }
        return this.#resolveTargetFromSelector(selector, waitForSelector);
    };

    #resolveTargetFromSelector = async (selector, waitForSelector) => {
        if (waitForSelector) {
            return this.page.waitForSelector(selector, {
                timeout: waitForSelector,
            });
        }
        return this.page.$(selector);
    };

    #resolveTargetFromXPathSelector = async (target, waitForSelector) => {
        if (waitForSelector) {
            return this.page.waitForXPath(selector, {
                timeout: waitForSelector,
            });
        }
        return this.page.$x(selector);
    };

    #moveMouseToPosition = async (x, y, options) => {
        if (options.immediate) {
            await this.page.mouse.move(x, y);
            this.lastPosition = { x, y };
            return { x, y };
        }
        this.lastPosition = await tracePath(
            this.page,
            path(this.lastPosition, { x, y })
        );
    };

    #getElementPosition = (element) => {
        return this.page.evaluate((element) => {
            const { top, left, bottom, right } =
                element.getBoundingClientRect();
            return { top, left, bottom, right };
        }, element);
    };

    #getMouseDestination = async (element) => {
        const { top, left, bottom, right } = await this.#getElementPosition(
            element
        );
        return {
            x: left + (right - left) / 2,
            y: top + (bottom - top) / 2,
            box: { top, left, bottom, right },
        };
    };

    #installMouseVisual = async () => {
        const vHandler = Buffer.from(visualHandler.toString()).toString(
            'base64'
        );
        let injectable = `window.addEventListener('DOMContentLoaded', HANDLER, false);`;
        injectable = injectable.replace(
            'HANDLER',
            `() => { eval(atob("${vHandler}")); visualHandler(); }`
        );
        return this.page.evaluateOnNewDocument(injectable);
    };
}
