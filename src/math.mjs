// Things in here are taken from: https://github.com/Xetera/ghost-cursor

import { Bezier } from 'bezier-js';

export const origin = { x: 0, y: 0 };

// maybe i should've just imported a vector library lol
export const sub = (a, b) => ({ x: a.x - b.x, y: a.y - b.y });
export const div = (a, b) => ({ x: a.x / b, y: a.y / b });
export const mult = (a, b) => ({ x: a.x * b, y: a.y * b });
export const add = (a, b) => ({ x: a.x + b.x, y: a.y + b.y });

export const direction = (a, b) => sub(b, a);
export const perpendicular = (a) => ({ x: a.y, y: -1 * a.x });
export const magnitude = (a) => Math.sqrt(Math.pow(a.x, 2) + Math.pow(a.y, 2));
export const unit = (a) => div(a, magnitude(a));
export const setMagnitude = (a, amount) => mult(unit(a), amount);
export const randomNumberRange = (min, max) =>
    Math.random() * (max - min) + min;

/**
 * Calculate the amount of time needed to move from (x1, y1) to (x2, y2)
 * given the width of the element being clicked on
 * https://en.wikipedia.org/wiki/Fitts%27s_law
 */
export function fitts(distance, width) {
    const a = 0;
    const b = 2;
    const id = Math.log2(distance / width + 1);
    return a + b * id;
}

export const randomVectorOnLine = (a, b) => {
    const vec = direction(a, b);
    const multiplier = Math.random();
    return add(a, mult(vec, multiplier));
};

const randomNormalLine = (a, b, range) => {
    const randMid = randomVectorOnLine(a, b);
    const normalV = setMagnitude(perpendicular(direction(a, randMid)), range);
    return [randMid, normalV];
};

export const generateBezierAnchors = (a, b, spread) => {
    const side = Math.round(Math.random()) === 1 ? 1 : -1;
    const calc = () => {
        const [randMid, normalV] = randomNormalLine(a, b, spread);
        const choice = mult(normalV, side);
        return randomVectorOnLine(randMid, add(randMid, choice));
    };
    return [calc(), calc()].sort((a, b) => a.x - b.x);
};

const clamp = (target, min, max) => Math.min(max, Math.max(min, target));

export const overshoot = (coordinate, radius) => {
    const a = Math.random() * 2 * Math.PI;
    const rad = radius * Math.sqrt(Math.random());
    const vector = { x: rad * Math.cos(a), y: rad * Math.sin(a) };
    return add(coordinate, vector);
};

export const bezierCurve = (start, finish, overrideSpread) => {
    // could be played around with
    const min = 2;
    const max = 200;
    const vec = direction(start, finish);
    const length = magnitude(vec);
    const spread = clamp(length, min, max);
    const anchors = generateBezierAnchors(
        start,
        finish,
        overrideSpread ?? spread
    );
    return new Bezier(start, ...anchors, finish);
};
